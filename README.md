# Ping Pong TCP with Java and Bazel

## A simple client-server application using the TCP protocol.

A client sends a packet of type "Ping", then if the packet is received by the server, it responds with a packet of type "Pong".
The response time is calculated and displayed on the screen.


## Installation

1. <a href="https://www.java.com/en/download/help/download_options.html" target="_blank">install java</a>
2. <a href="https://bazel.build/install" target="_blank">install bazel</a>
3. clone this project
4. In the root of the project run with bazel - `bazel run //src/main/java/org/example:main`
